![Keka](http://ice-kylin.gitee.io/icekylinfigurebed/images/repository/Keka/Keka.png)

# Keka

> macOS 压缩文件管理器

便捷存储，共享安全

## 便捷且强大

![Dock](http://ice-kylin.gitee.io/icekylinfigurebed/images/repository/Keka/Keka-dock.png)

您在使用时甚至无需打开 Keka，只需把 Keka 放到 Dock 中，  
然后将文件和文件夹拖放到 Dock 上的 Keka 图标或窗口即可快速创建压缩文件

## 注重隐私安全

![Password](http://ice-kylin.gitee.io/icekylinfigurebed/images/repository/Keka/Keka-password.png)

安全的共享文件只需通过设置密码来创建安全加密的压缩文件即可  
为您的 7z 文件使用 AES-256 加密规范，为 Zip 文件使用 Zip 2.0 传统加密规范

## 压缩结果仍然过大...

![Split](http://ice-kylin.gitee.io/icekylinfigurebed/images/repository/Keka/Keka-split.png)

如果文件实在太大并且它们不适用于邮件发送或网络传输，请将它们分卷压缩  
不用担心，它们依然可以解压出原来的文件 :)

## 支持什么类型？

Keka 可以创建以下格式的压缩文件：  
7Z ZIP TAR GZIP BZIP2 XZ LZIP DMG ISO

并支持解压这些格式：  
7Z ZIP ZIPX RAR TAR GZIP BZIP2 XZ LZIP DMG ISO LZMA EXE CAB WIM PAX JAR WAR IPA APK APPX XPI CPGZ CPIO

## 下载 Keka

如果您喜欢 Keka，您可以从 App Store 购买 Keka 或通过 PayPal 发起捐赠~

如果您从 App Store 购买 Keka，这会支持我们的开发工作，  
App Store 中的版本与本仓库中下载的版本是相同的

### 二进制安装包

请在 [releases](https://gitee.com/ice-kylin/Keka/tree/master/releases) 文件夹中选择已经编译好的适当版本二进制安装包下载

## 关于此仓库…

为方便内地用户下载 Keka,建立此仓库

官方仓库地址： https://github.com/aonez/Keka  
官方wiki页面： https://github.com/aonez/Keka/wiki  
官方中文网站： https://www.keka.io/zh-cn  
官方英文网站： https://www.keka.io  

***

© 2009 - 2019 aone. 版权所有