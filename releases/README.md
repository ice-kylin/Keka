![Keka](http://ice-kylin.gitee.io/icekylinfigurebed/images/repository/Keka/Keka.png)

# 版本说明

## 最新版本

### v1.1.25

22.6 MB | 需要 Mac OS X 10.9 或更高版本  
MD5：92bf9781461db977aee83d2af9f5eaa0

## 测试版本

> 您可以在正式版发布之前测试 Keka 的最新功能  
> 如果您发现了错误或要提交某些内容，请转到[缺陷](https://issues.keka.io)

### v1.2.0-dev.3742

32.1 MB | 需要 Mac OS X 10.9 或更高版本  
MD5：102691d477db4d3b4d4b895e8fb701dc

## 历史版本

> 多年后您的 Mac 设备将变得陈旧，或许不会支持最新版本的 Keka  
> 别担心，所有 Keka 历史版本都在这儿

### v1.0.16

19.2 MB | 适用于 Mac OS X 10.7  
MD5：e5bad4030c40be92f38f809d88ea05ae

### v1.0.4

19.3 MB | 适用于 Mac OS X 10.6  
MD5：cf38fc24211f69f42dc4eaebf41c2d0a

### v1.0.4-leo

17.1 MB | 适用于 Mac OS X 10.5  
MD5：11618415e93059cead2cf68e6bcf4aed

### v0.1.2.1

4.7 MB | 适用于 Mac OS X 10.4  
MD5：5b7bb96bbf2fb0987b6796ac4a4cd64f

***

© 2009 - 2019 aone. 版权所有